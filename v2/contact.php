<?php include "_header.php"; ?>

  <section class="contact-section" id="contact">
    <div class="container">
      <div class="section-title">
        <h1>Contact</h1>
      </div>
      <div class="tagline text-red">
        For further information and advice, please feel free to contact us at the following address:
      </div>
      <div class="gmap-wrapper">
        <iframe width="100%" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" zoom="21" src="https://maps.google.com/maps?q=13.735552,100.563268&amp;num=1&amp;ie=UTF8&amp;ll=13.735469,100.562931&amp;spn=0.001751,0.00284&amp;t=m&amp;z=14&amp;output=embed"></iframe>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="contact-info-wrapper">
            <h2>Contact Info</h2>
            <div class="row">
              <div class="col-md-6">
                <strong>Professional Auditing Service Company Limited</strong>
                <p>
                  1 Glas Haus Building, 9 Fl. Room 905/2, Soi Sukhumvit 25, Sukhumvit Road,  Klongtoey Nua, Wattana,  Bangkok 10110
                </p>
              </div>
              <div class="col-md-6">
                <div>
                  <strong>Tel:</strong> +66 02 261 1785, +66 02 261 1786
                </div>
                <div>
                  <strong>Fax:</strong> +66 02 261 1784
                </div>
                <div>
                  <strong>Email:</strong> <a href="mailto:natthapol@aapth.com">natthapol@aapth.com</a>
                </div>
                <div>
                  <strong>Contact:</strong> Mr. Natthapol Musikaparn
                </div>
              </div>
            </div>
          </div>
          <!-- /.contact-info-wrapper -->
        </div>
        <!-- /.col-md-8 -->
        <div class="col-md-4">
          <div class="contact-form-wrapper">
            <h2>Get In Touch</h2>
            <div id="success" class="alert alert-success" style="display:none">Your email has been sent. Thank you, we will get back to you soon.</div>
              <div id="error" class="alert alert-danger" style="display:none">alert</div>
            <form id="contact_form">
              <div class="form-group">
                <label for="">Name:</label>
                <input type="text" id="name" class="form-control" placeholder="Enter your name">
              </div>
              <div class="form-group">
                <label for="">Email:</label>
                <input type="text" id="email" class="form-control" placeholder="Enter your email">
              </div>
              <div class="form-group">
                <label for="">Message:</label>
                <textarea id="message" rows="4" cols="40" class="form-control" placeholder="Enter your message"></textarea>
              </div>
              <div class="form-group webmaster">
                <strong>contact webmaster:</strong> <a href="mailto:chaiwat@aapth.com">chaiwat@aapth.com</a>
              </div>
              <div class="form-submit">
                <button type="submit" class="pas-submit" id="submit_contact_info">Send</button>
              </div>
            </form>
          </div>
          
        </div>
        <!-- /.col-md-4 -->
      </div>
      </div>
  </section>
  <!-- /.contact-section -->

<?php include "_footer.php"; ?>
