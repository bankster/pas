<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>Professional Auditing Service Company Limited</title>
  <meta name="description" content="">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="css/easy-responsive-tabs.css">
  <link rel="stylesheet" href="css/scrollup-image.css">
  <link rel="stylesheet" href="css/main.css">

  <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>

<body id="top">
  <div class="navbar navbar-default navbar-fixed-top">
    <div class="site-name-wrapper">
      <div class="container">
        <div class="site-name pull-right">
          <img src="img/pas-logo.png" alt="P.A.S. Audit &amp; Assurance Service" height="60px">
        </div>
      </div>
    </div>
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav pas-nav navbar-right" id="menu">
          <li>
            <a href="index.php">Home</a>
          </li>
          <li>
            <a href="service.php">Service</a>
          </li>
          <li>
            <a href="career.php">Career</a>
          </li>
          <li>
            <a href="links.php">Links</a>
          </li>
          <li>
            <a href="contact.php">Contact</a>
          </li>
        </ul>
      </div>
      <!--/.navbar-collapse -->
    </div>
  </div>