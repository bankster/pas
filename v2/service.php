<?php include "_header.php"; ?>

  <section class="service-section" id="service">
    <div class="container">
      <div class="section-title">
        <h1>Our Services</h1>
      </div>
      <p class="tagline text-red">
        “Give better than loss, the moments of the trust.”
      </p>
      <p>
        Best friend for us, all please to advise them in strategies, plans and business advisors. As the leading provide professional services firm, the trust and the value of business, investment and the financial are earned over more than the business engagement.
      </p>
      <p>
        Not reason and no matter to change the trust. We can help you reach your goals and over the chances
      </p>
      <div class="service-tab" id="respTab">
        <ul class="resp-tabs-list">
          <li>
            <a href="#service2" data-toggle="tab">Audit &amp; Assurance</a>
          </li>
          <li>
            <a href="#service3" data-toggle="tab">Tax Advisory Services</a>
          </li>
          <li>
            <a href="#service1" data-toggle="tab">Accounting Service</a>
          </li>
          <li>
            <a href="#service4" data-toggle="tab">Business Advisory Services</a>
          </li>
        </ul>
        <!-- /.resp-tabs-list -->
        <div class="resp-tabs-container">
          <div id="service2">
            <h2 class="text-red resp-tabs-title">Audit &amp; Assurance</h2>
            <p>
              We are experts consultant and professional audit team in providing about audit &amp; assurance services in all types of business including companies in the under of the International Finance Reporting Standard (IFRS).
            </p>
            <ul>
              <li>Accounting advisory</li>
              <li>Financial statement audit</li>
              <li>Internal audit</li>
              <li>Risk assurance</li>
              <li>Regulatory compliance &amp; reporting</li>
              <li>Corporate reporting &amp; treasury solutions</li>
              <li>Actuarial insurance  services</li>
              <li>IFRS reporting</li>
            </ul>
          </div>
          <div id="service3">
            <h2 class="text-red resp-tabs-title">Tax Advisory Services</h2>
            <p>
              Our Tax Services include as a liaison to the state revenue / tax office for corporate and individual.
            </p>
            <p>
              This Includes assistance on tax objections of the company by preparing the necessary documentation.
            </p>
            <ul>
              <li>Tax Management &amp; Planning.</li>
              <li>Tax Compliance Audit.</li>
              <li>Tax Consultant.</li>
              <li>Tax Preparation &amp; Filling.</li>
            </ul>
          </div>
          <div id="service1">
            <h2 class="resp-tabs-title">Accounting Service</h2>
            <p>
              We provide accounting services for a large number of clients who do not find it commercially viable to employ a full time accountant. The Firm provide a high standard of accounting services to ensure that clients receive the best possible advice and comply with the requirements of the various statutory authorities.
            </p>
            <p>
              We are committed to provide timely and reliable financial reporting for the needs of our clients.
            </p>
            <p>
              Our services include:
            </p>
            <ul>
              <li>Preparation of periodical and annual financial statements</li>
              <li>Maintain accounting records for statutory and taxation audit purposes.</li>
            </ul>
          </div>
          <div id="service4">
            <h2 class="text-red resp-tabs-title">Business Advisory Services</h2>
            <p>
              In the present in globalization, <strong>“Business need to work smarter and grow faster.”</strong>
            </p>
            <p>
              For the convenience,  we consult with our clients to build the effective with organizations  to innovate &amp; grow, reduce costs, manage risk &amp; regulation  and  leverage talent from resource of business.
            </p>
            <p>
              Our vision to be support your business in designing, managing and executing lasting to better beneficial change.
            </p>
            <ul>
              <li>Strategy Management</li>
              <li>Customs &amp; Trade.</li>
              <li>Operations</li>
              <li>Service outsourcing</li>
              <li>Long-term development</li>
              <li>Corporate finance</li>
              <li>Tax accounting services.</li>
              <li>Financial Advisory Services.</li>
              <li>Internal Audit.</li>
              <li>Financial due diligence</li>
              <li>Mergers &amp; acquisitions</li>
              <li>Assets management</li>
              <li>Valuations and economics</li>
              <li>Logic risk engagement.</li>
              <li>Financial Services.</li> 
              <li>Human resource value.</li>
            </ul>
          </div>
        </div>
        <!-- /.resp-tabs-container -->
      </div>
    </div>
  </section>
  <!-- /.service-wrapper -->

<?php include "_footer.php"; ?>
