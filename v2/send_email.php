<?php

if ( ! $_POST ) {
	header('Location: http://pasaudit.com/');
	exit;
}
// var_dump($_POST); exit;

if ( isset($_POST['data']) && !empty($_POST['data']) )
{
	foreach ($_POST['data'] as $key => $value)
	{
		$data[$value[0]] = $value[1];
	}

	if ( isset($data['name']) && !empty($data['name']) 
		&& isset($data['email']) && !empty($data['email'])
		&& isset($data['message']) && !empty($data['message']) )
	{
		$send_to = 'contact@pasaudit.com';
		// subject
		$subject = "Message from {$data['name']}";
		// set email message
		$msg = 		"From: {$data['name']} \n";
		$msg .= 	"Email: {$data['email']} \n";

		$msg .= 	"Message: {$data['message']} \n";
		$msg .= 	"---\n อีเมล์นี้ถูกส่งผ่านทางแบบฟอร์มติดต่อบน pasaudit.com";

		// set header
		$header = "From: system@pasaudit.com";
		
		// send email
		if ( @mail( $send_to, $subject, $msg, $header ) )
		{
			$response = 0;
		}
		else
		{
			$response = "Error! : Can't send email, Please try again.";
		}
	}
	else
	{
		$response = 'Please Insert all the required Fields! (*)';
	}
}
else
{
	$response = 'Invalid data!)';
}

echo $response;

?>