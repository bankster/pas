<?php include "_header.php"; ?>

  <section class="career-section" id="career">
    <div class="container">
      <div class="section-title">
        <h1>Career</h1>
      </div>
      <p class="tagline">
        <strong class="text-red">Opportunity to Chance,</strong>
      </p>
      <p class="tagline text-red">
        If you are interested in joining us, we are always looking for someone have confidence &amp; talent individuals.
      </p>
      <p>
        At PAS, we invest in our people. We have in house training for staff ensuring that our staff are well informed and kept up-to-date with current development in accounting standards and other related issues. With our rigorous training programs, our staff stand a better chance of passing their CPA exams, and are able to move quickly in their professional career.
      </p>
      <p>
        Interested in joining us? Please send your resume in ENGLISH or THAI together with recent photo and work experience to call to office at 02-2611785 and 02-2611786
      </p>
      <div class="row">
        <div class="col-xs-6">
          <img src="img/career_photo_1.jpg" alt="" class="img-responsive">
        </div>
        <div class="col-xs-6">
          <img src="img/career_photo_2.jpg" alt="" class="img-responsive">
        </div>
      </div>
    </div>
  </section>
  <!-- /.career-section -->

<?php include "_footer.php"; ?>
