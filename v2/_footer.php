  <footer class="site-footer">
    <div class="container">
      <div class="site-footer-copyright">
        &copy; 2014 Professional Auditing Service Company Limited
      </div>
      <div class="site-footer-detail">
        The information contained herein has been obtained from sources we believed to be reliable but we do not make any representation or warranty nor accept any responsibility or liability as to its accuracy,completeness or correctness. Whilst we have taken all reasonable care to ensure that the information contained in this publication is not untrue or misleading at the time of publication, we cannot guarantee its accuracy or completeness, and you should not act on it without first independently verifying its contents.
      </div>
    </div>
  </footer>

  <script src="js/vendor/jquery-1.11.0.min.js"></script>
  <script src="js/vendor/jquery.easing.js"></script>
  <script src="js/vendor/bootstrap.min.js"></script>
  <script src="js/vendor/easyResponsiveTabs.js"></script>
  <script src="js/vendor/jquery.parallax-1.1.3.js"></script>
  <script src="js/vendor/jquery.localscroll-1.2.7-min.js"></script>
  <script src="js/vendor/jquery.scrollTo-1.4.13-min.js"></script>
  <script src="js/vendor/jquery.scrollUp.min.js"></script>

  <script src="js/plugins.js"></script>
  <script src="js/main.js"></script>
</body>

</html>