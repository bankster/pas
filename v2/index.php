<?php include "_header.php"; ?>

  <section class="hero-section">
    <div class="container">
      <div class="col-md-5">
        <div class="hero-text">
          <div class="logo-wrapper">
            <img src="img/kt-logo.png" alt="KRESTON LOGO">
            <img src="img/pas-logo.png" alt="PAS Audit &amp; Assurance Service LOGO">
          </div>
          <!-- /.logo-wrapper -->
          <div class="welcome-wrapper">
            <div class="welcome-text welcome-text-lg text-red"><strong>Welcome to</strong></div>
            <div class="welcome-text"><strong class="text-red">Professional Auditing Service Co., Ltd.</strong>
  </div>
            <div class="welcome-text text-red">Member of Kreston International, 
  </div>
            <div class="welcome-text welcome-text-sm">The Global Networks of Professional Firms.</div>
          </div>
        </div><!-- /.hero-text -->
      </div>
      <div class="col-md-8">
        <!-- <img src="img/cover.jpg" alt="PHOTO" class="img-responsive"> -->
      </div>
    </div>
    <!-- /.container -->

    <div class="partner-logo">
      <div class="container">
        <div class="partner-content">
          <a href="http://www.fap.or.th" target="_blank">
            <img src="img/fap.jpg" alt="" class="img-circle img-respontive" >
          </a>
          <a href="http://www.customs.go.th" target="_blank">
            <img src="img/customs.jpg" alt="" class="img-circle img-respontive" >
          </a>
          <a href="http://www.rd.go.th" target="_blank">
            <img src="img/rd.jpg" alt="" class="img-circle img-respontive" >
          </a>
          <a href="http://www.bot.or.th" target="_blank">
            <img src="img/bot.jpg" alt="" class="img-circle img-respontive" >
          </a>
          <a href="http://www.dbd.go.th" target="_blank">
            <img src="img/dbd.jpg" alt="" class="img-circle img-respontive" >
          </a>
          <a href="http://www.boi.go.th" target="_blank">
            <img src="img/boi.jpg" alt="" class="img-circle img-respontive" >
          </a>
          <a href="http://www.sec.or.th/" target="_blank">
            <img src="img/sec.jpg" alt="" class="img-circle img-respontive" >
          </a>
          <a href="http://www.set.or.th/" target="_blank">
            <img src="img/set.jpg" alt="" class="img-circle img-respontive" >
          </a>
        </div>
      </div>
    </div>
    <!-- /.partner-logo -->
  </section>
  <!-- ./hero-wrapper -->

  <div class="clearfix"></div>

  <section class="about-section" id="about">
    <div class="container">
      <div class="section-title">
        <h1>About Us</h1>
      </div>
      <p>
        <strong class="text-red">Professional Auditing Service Co., Ltd (PAS)</strong> is members of Kreston International Limited, a global network of independent accounting firms. Founded in 1971 we offer reliable and convenient access to quality services through member firms located around the globe. Currently ranking as the 13th largest accounting network in the world, Kreston now covers 108 countries with 186 firms providing a resource of over 20, 000 professional and support staff.
      </p>
      <p>
        In today's economic climate, with rapidly developing new markets, improved communications, and greater international mobility, businesses operate on an increasingly global scale. Successful global organisations choose to use the services of accountants and advisors in each individual country, to take advantage of their local expertise and contacts.
      </p>
      <p>
        Kreston International member firms commit to compliance with the professional standards appropriate in their respective countries and to adhere to the following international standards:
      </p>
      <ul>
        <li>International Standards on Quality Control</li>
        <li>International Standards on Auditing for the conduct of transnational audits.</li>
        <li>Code of Ethics as issued by the International Ethics Standards Board for Accountants.</li>
      </ul>
      <p>
        A globally coordinated quality monitoring and review programme supports member firms in the maintenance of these standards.
      </p>

      <div class="clearfix"></div>

      <div class="partner-logo-link">
        <a href="http://www.kreston.com/" target="_blank">
          <img src="img/kreston-ilogo.png" alt="kreston international" class="img-responsive">
        </a>
      </div>

    </div>
  </section>
  <!-- /.about-wrapper -->

<?php include "_footer.php"; ?>
