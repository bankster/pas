// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
$('#respTab').easyResponsiveTabs({
  type: 'vertical', //Types: default, vertical, accordion
});

$(window).load(function() {
  $('#menu').localScroll({offset:-50,duration:1500});
  $('#scrollNav').localScroll({offset:-50,duration:1500});

  //parallax
  $('#top').parallax("50%", 0.3);
  $('#about').parallax("50%", 0.3);
  $('#service').parallax("50%", 0.3);
  $('#team').parallax("50%", 0.3);
  $('#gallery').parallax("50%", 0.3);
  $('#career').parallax("50%", 0.3);
  $('#contact').parallax("50%", 0.3);

  $.scrollUp({
    scrollSpeed: 1500,
    scrollText: '',
    easingType: 'easeOutCirc'
  });
});