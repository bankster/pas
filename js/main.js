$(document).ready(function() {
	/* Forms */
    $('#submit_contact_info').click(function(e){
		e.preventDefault();
		data = [];
		$('#contact_form input, #contact_form textarea').each(function(){
			data.push([$(this).attr('id'), $(this).val()]);
		});
		console.log(data);
		$.post('send_email.php', {'data':data}, function(response){
			if(response != '0'){
				$('#error').hide();
				$('#error').html(response);
				$('#error').slideDown();
			} else {
				$('#error').hide();
				$('#contact_form').hide();
				$('#success').slideDown();
				//$('.after-close').show();
			}
		});
	});
});
